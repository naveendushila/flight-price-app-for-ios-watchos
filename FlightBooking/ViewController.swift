//
//  ViewController.swift
//  FlightBooking
//
//  Created by Naveen Dushila on 2019-03-13.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    
    @IBOutlet weak var cityFrom: DesignableTextField!
    
      var pickerView = UIPickerView()
      var cityList: [String] = ["Barrie","Belleville","Brampton","Brant","Brantford","Brockville","Burlington","Cambridge","Clarence-Rockland","Cornwall","Dryden","Elliot Lake","Greater Sudbury","Guelph","Haldimand County","Hamilton","Kawartha Lakes","Kenora","Kingston","Kitchener","London","Markham","Mississauga","Niagara Falls","Norfolk County","North Bay","Orillia","Oshawa","Ottawa","Owen Sound","Pembroke","Peterborough","Pickering","Port Colborne","Prince Edward County","Quinte West","Sarnia","Sault Ste. Marie","St. Catharines","St. Thomas","Stratford","Temiskaming Shores","Thorold","Thunder Bay","Timmins","Toronto","Vaughan","Waterloo","Welland","Windsor","Woodstock"]
    
    
    
    var selectedCityIndex : Int = 0
    
    var urlAccessToken = "https://api.sunrise-sunset.org/json?lat=49.2827&lng=-123.1207&date=today"
    var acccessToken = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        cityFrom.inputView = pickerView
        cityFrom.textAlignment = .left

    }
    
    func numberOfComponents(in pickerView:UIPickerView) -> Int{
        return 1
    }
    func pickerView(_ pickerView: UIPickerView ,numberOfRowsInComponent component: Int) -> Int {
        return self.cityList.count
    }
    
    func pickerView(_ pickerView: UIPickerView ,titleForRow row : Int , forComponent component : Int) -> String?{
        return self.cityList[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        cityFrom.text = cityList[row]
        cityFrom.resignFirstResponder()
    }
    

}


